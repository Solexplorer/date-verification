from icalendar import Calendar
from datetime import datetime
import io
import requests


def result_date(date_check, year):
    """
        input: data_check of type str
               year of type int
        purpose: prints information whether a day is a holiday
    """
    #Endpoint only has calendars from 2012 to 2020
    if 2012 <= year <= 2020:
        calendar = requests.get(f"https://giorni-festivi.eu/ical/italia/{year}").text
        # Convert response to file object so that I can parse it
        ics_obj = io.StringIO(calendar)

        gcal = Calendar.from_ical(ics_obj.read())
        # Iterate each event in the calendar and check if the input date is a holiday
        for component in gcal.walk():
            if component.name == "VEVENT":
                converted_date = datetime.strftime(component.get('dtstart').dt, '%d-%m-%Y')
                if converted_date == date_check:
                    print(f"La data inserita è un giorno festivo nazionale: {component.get('summary')}")
                    return

    # Convert input date to datetime object
    dt_object = datetime.strptime(date_check, '%d-%m-%Y')
    if dt_object.strftime("%A") == 'Sunday':
        print("La data inserita è un giorno festivo")
    elif dt_object.strftime("%A") == 'Saturday':
        print("La data inserita è un giorno prefestivo")
    else:
        print("La data inserita è un giorno lavorativo")


def main():
    """
        main-program
        purpose: handles user input and prints
                 information to the console.
    """

    while True:
        date_check = input('\nInserisci la data da controllare\n')
        try:
            datetime.strptime(date_check, '%d-%m-%Y')
        except ValueError:
            print("Data non nel formato richiesto (dd-mm-yyyy)")
            continue

        year = int(date_check[-4:])
        result_date(date_check, year)


if __name__ == '__main__':
    main()
